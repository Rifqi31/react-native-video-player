import React from 'react';
import { AppRegistry, StyleSheet, View } from 'react-native';
import Video from 'react-native-video';

export default class App extends React.Component {
  render() {
    return(
      <View style={styles.container}>
        <Video 
          source={require('./src/redribbon.mp4')}
          rate={1.0}
          volume={1.0}
          muted={false}
          // resizeMode={"cover"}
          repeat
          controls={true}
          style={styles.video}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000"
  },
  video: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});

AppRegistry.registerComponent('App', () => App);
